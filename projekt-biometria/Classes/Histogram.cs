﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace projekt_biometria.Classes
{
    public partial class Histogram
    {
        public PointCollection luminanceHistogramPoints = null;
        public PointCollection redColorHistogramPoints = null;
        public PointCollection greenColorHistogramPoints = null;
        public PointCollection blueColorHistogramPoints = null;
        public bool PerformHistogramSmoothing { get; set; }
        public byte[] changePixels;
        

        public Histogram(byte[] pix)
        {
            this.changePixels = pix;
            Start();
        }

        public void Start()
        {
            luminanceHistogramPoints = ConvertToPointCollection(GetHistogramAverageValues());
            redColorHistogramPoints = ConvertToPointCollection(GetHistogramRedValues());
            greenColorHistogramPoints = ConvertToPointCollection(GetHistogramGreenValues());
            blueColorHistogramPoints = ConvertToPointCollection(GetHistogramBlueValues());
        }

        private PointCollection ConvertToPointCollection(int[] values)
        {
            int max = values.Max();
            PointCollection points = new PointCollection();
            points.Add(new Point(0, max));
            for (int i = 0; i < values.Length; i++)
            {
                points.Add(new Point(i, max - values[i]));
            }
            points.Add(new Point(values.Length - 1, max));
            return points;
        }

        public int[] GetHistogramAverageValues()
        {
            int[] averageValues = new int[256];
            for (int i = 0; i < changePixels.Length; i += 4)
            {
                int value = (changePixels[i] + changePixels[i + 1] + changePixels[i + 2]) / 3;
                averageValues[value]++;
            }

            return averageValues;

        }

        public int[] GetHistogram()
        {
            int[] histogram = new int[256];

            for (int i = 0; i < changePixels.Length; i++)
            {
                histogram[changePixels[i]]++;
            }
            return histogram;
        }

        public int[] GetHistogramRedValues()
        {
            int[] redValues = new int[256];

            for (int i = 2; i < changePixels.Length; i += 4)
            {
                redValues[changePixels[i]]++;
            }

            return redValues;
        }

        public int[] GetHistogramGreenValues()
        {
            int[] greenValues = new int[256];

            for (int i = 1; i < changePixels.Length; i += 4)
            {
                greenValues[changePixels[i]]++;
            }

            return greenValues;
        }

        public int[] GetHistogramBlueValues()
        {
            int[] blueValues = new int[256];

            for (int i = 0; i < changePixels.Length; i += 4)
            {
                blueValues[changePixels[i]]++;
            }

            return blueValues;
        }


    }
}
