﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using projekt_biometria.Classes;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using Point = System.Windows.Point;
using Region = projekt_biometria.Classes.Region;
using Colors = projekt_biometria.Classes.Colors;

namespace projekt_biometria
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged, IDataErrorInfo
    {
        #region Variables

        private WriteableBitmap _bitmap;
        private BitmapImage _image;
        private ImageProperties _imageProperties;
        private ImageOperations _imageOperations;
        private byte[] _pixels, _changedPixels;
        private int _width;
        private int _height;
        private int _stride;
        private int _arraySize;
        private string _source;
        private string _r;
        private string _g;
        private string _b;
        private string _r1;
        private string _g1;
        private string _b1;
        private string _xPixel;
        private string _yPixel;
        private string _aa;
        private string _bb;
        private Point origin;
        private Point start;
        private string _brightness = "1";
        private string _brightness2 = "1";
        Histogram _histogram;
        public byte[] changePixels;
        private string _binarizationThreshold;
        private string _frame;
        private int[,] MaskTable = new int[3, 3];
        private string _w1;
        private string _w2;
        private string _w3;
        private string _w4;
        private string _w5;
        private string _w6;
        private string _w7;
        private string _w8;
        private string _w9;
        private string _maskSize;

        public PointCollection luminanceHistogramPoints = null;
        public PointCollection redColorHistogramPoints = null;
        public PointCollection greenColorHistogramPoints = null;
        public PointCollection blueColorHistogramPoints = null;

        public PointCollection LuminanceHistogramPoints
        {
            get { return this.luminanceHistogramPoints; }
            set
            {
                this.luminanceHistogramPoints = value;
                this.OnPropertyChanged("LuminanceHistogramPoints");
            }
        }

        public PointCollection RedColorHistogramPoints
        {
            get { return this.redColorHistogramPoints; }
            set
            {
                if (this.redColorHistogramPoints != value)
                {
                    this.redColorHistogramPoints = value;
                    this.OnPropertyChanged("RedColorHistogramPoints");
                }
            }
        }

        public PointCollection GreenColorHistogramPoints
        {
            get { return this.greenColorHistogramPoints; }
            set
            {
                this.greenColorHistogramPoints = value;
                this.OnPropertyChanged("GreenColorHistogramPoints");
            }
        }

        public PointCollection BlueColorHistogramPoints
        {
            get { return this.blueColorHistogramPoints; }
            set
            {

                this.blueColorHistogramPoints = value;
                this.OnPropertyChanged("BlueColorHistogramPoints");

            }
        }

        public string Source
        {
            get { return _source; }
            set
            {
                _source = value;
                OnPropertyChanged("Source");
            }
        }

        public WriteableBitmap Bitmap
        {
            get { return _bitmap; }
            set
            {
                _bitmap = value;
                OnPropertyChanged("Bitmap");
            }
        }

        public string R
        {
            get { return _r; }
            set
            {
                _r = value;
                OnPropertyChanged("R");
            }
        }

        public string G
        {
            get { return _g; }
            set
            {
                _g = value;
                OnPropertyChanged("G");
            }
        }

        public string B
        {
            get { return _b; }
            set
            {
                _b = value;
                OnPropertyChanged("B");
            }
        }

        public string R1
        {
            get { return _r1; }
            set
            {
                _r1 = value;
                OnPropertyChanged("R1");
            }
        }

        public string G1
        {
            get { return _g1; }
            set
            {
                _g1 = value;
                OnPropertyChanged("G1");
            }
        }

        public string B1
        {
            get { return _b1; }
            set
            {
                _b1 = value;
                OnPropertyChanged("B1");
            }
        }

        public string XPixel
        {
            get { return _xPixel; }
            set
            {
                _xPixel = value;
                OnPropertyChanged("XPixel");
                if (XYPixelValidation()) ShowPixelRGB();
            }
        }

        public string YPixel
        {
            get { return _yPixel; }
            set
            {
                _yPixel = value;
                OnPropertyChanged("YPixel");
                if (XYPixelValidation()) ShowPixelRGB();
            }
        }

        public string Brightness
        {
            get { return _brightness; }
            set
            {
                _brightness = value;
                OnPropertyChanged("Brightness");
            }
        }

        public string Brightness2
        {
            get { return _brightness2; }
            set
            {
                _brightness2 = value;
                OnPropertyChanged("Brightness2");
            }
        }

        public string AA
        {
            get { return _aa; }
            set
            {
                _aa = value;
                OnPropertyChanged("AA");
            }
        }

        public string BB
        {
            get { return _bb; }
            set
            {
                _bb = value;
                OnPropertyChanged("BB");
            }
        }

        public string BinarizationThreshold
        {
            get { return _binarizationThreshold; }
            set
            {
                _binarizationThreshold = value;
                OnPropertyChanged("BinarizationThreshold");
            }
        }

        public string Frame
        {
            get { return _frame; }
            set
            {
                _frame = value;
                OnPropertyChanged("Frame");
            }
        }

        public string W1
        {
            get { return _w1; }
            set
            {
                _w1 = value;
                OnPropertyChanged("W1");
            }
        }

        public string W2
        {
            get { return _w2; }
            set
            {
                _w2 = value;
                OnPropertyChanged("W2");
            }
        }

        public string W3
        {
            get { return _w3; }
            set
            {
                _w3 = value;
                OnPropertyChanged("W3");
            }
        }

        public string W4
        {
            get { return _w4; }
            set
            {
                _w4 = value;
                OnPropertyChanged("W4");
            }
        }

        public string W5
        {
            get { return _w5; }
            set
            {
                _w5 = value;
                OnPropertyChanged("W5");
            }
        }

        public string W6
        {
            get { return _w6; }
            set
            {
                _w6 = value;
                OnPropertyChanged("W6");
            }
        }

        public string W7
        {
            get { return _w7; }
            set
            {
                _w7 = value;
                OnPropertyChanged("W7");
            }
        }

        public string W8
        {
            get { return _w8; }
            set
            {
                _w8 = value;
                OnPropertyChanged("W8");
            }
        }

        public string W9
        {
            get { return _w9; }
            set
            {
                _w9 = value;
                OnPropertyChanged("W9");
            }
        }

        public string MaskSize
        {
            get { return _maskSize; }
            set
            {
                _maskSize = value;
                OnPropertyChanged("MaskSize");
            }
        }

        #endregion

        #region Commands

        private void FilteringEndings(object sender, ExecutedRoutedEventArgs e)
        {
            var LUT = GetLUTForBinarization(100);
            ChangePictureWithLUT(LUT);
            _imageProperties.ChangedPixels = _changedPixels;
            var points = Minutiae.FindRidgeEndings(_imageProperties, true);
            MinutiaeDecoration(points, 15);
        }

        private void CanExecuteFilteringEndings(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void FindingEndings(object sender, ExecutedRoutedEventArgs e)
        {
            var LUT = GetLUTForBinarization(100);
            ChangePictureWithLUT(LUT);
            _imageProperties.ChangedPixels = _changedPixels;
            var points = Minutiae.FindRidgeEndings(_imageProperties, false);
            MinutiaeDecoration(points, 15);
        }

        private void CanExecuteFindingEndings(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void FindingBranches(object sender, ExecutedRoutedEventArgs e)
        {
            var LUT = GetLUTForBinarization(100);
            ChangePictureWithLUT(LUT);
            _imageProperties.ChangedPixels = _changedPixels;
            var points = Minutiae.FindBranching(_imageProperties);
            MinutiaeDecoration(points, 15);
        }

        private void CanExecuteFindingBranches(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void PictureThinning(object sender, ExecutedRoutedEventArgs e)
        {
            var t = _imageOperations.Image2Bool();
            //t = Thinning.ZhangSuenThinning(t);
            t = Thinning.KMMMThinning(t);
            _imageProperties.ChangedPixels = _imageOperations.Bool2Image(t);
            _changedPixels = _imageProperties.ChangedPixels;
            DrawChangePicture(_imageProperties.ChangedPixels);
        }

        private void CanExecutePictureThinning(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void CornerDetectingFilter(object sender, ExecutedRoutedEventArgs e)
        {
            W1 = "1";
            W2 = "1";
            W3 = "1";
            W4 = "1";
            W5 = "-2";
            W6 = "-1";
            W7 = "1";
            W8 = "-1";
            W9 = "-1";
        }

        private void CanExecuteCornerDetectingFilter(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void BlurFilter(object sender, ExecutedRoutedEventArgs e)
        {
            W1 = "1";
            W2 = "1";
            W3 = "1";
            W4 = "1";
            W5 = "1";
            W6 = "1";
            W7 = "1";
            W8 = "1";
            W9 = "1";
        }

        private void CanExecuteBlurFilter(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void LaplaceFilter(object sender, ExecutedRoutedEventArgs e)
        {
            W1 = "0";
            W2 = "-1";
            W3 = "0";
            W4 = "-1";
            W5 = "4";
            W6 = "-1";
            W7 = "0";
            W8 = "-1";
            W9 = "0";
        }

        private void CanExecuteLaplaceFilter(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void SobelFilter(object sender, ExecutedRoutedEventArgs e)
        {
            W1 = "-1";
            W2 = "-2";
            W3 = "-1";
            W4 = "0";
            W5 = "0";
            W6 = "0";
            W7 = "1";
            W8 = "2";
            W9 = "1";
        }

        private void CanExecuteSobelFilter(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void PrewittFilter(object sender, ExecutedRoutedEventArgs e)
        {
            W1 = "-1";
            W2 = "-1";
            W3 = "-1";
            W4 = "0";
            W5 = "0";
            W6 = "0";
            W7 = "1";
            W8 = "1";
            W9 = "1";
        }

        private void CanExecutePrewittFilter(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void FilterSettings(object sender, ExecutedRoutedEventArgs e)
        {
            filterPanel.Visibility = Visibility.Visible;
            binarizationNiblack.Visibility = Visibility.Hidden;
            binarizationPanel.Visibility = Visibility.Hidden;
            stretchingPanel.Visibility = Visibility.Hidden;
            brightnessPanel.Visibility = Visibility.Hidden;
            histogramPanel.Visibility = Visibility.Hidden;
            W1 = "0";
            W2 = "0";
            W3 = "0";
            W4 = "0";
            W5 = "0";
            W6 = "0";
            W7 = "0";
            W8 = "0";
            W9 = "0";
            MaskSize = "3";
        }

        private void CanExecuteFilterSettings(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void LinearFilter(object sender, ExecutedRoutedEventArgs e)
        {
            MaskTable[0, 0] = Int32.Parse(W1);
            MaskTable[0, 1] = Int32.Parse(W2);
            MaskTable[0, 2] = Int32.Parse(W3);
            MaskTable[1, 0] = Int32.Parse(W4);
            MaskTable[1, 1] = Int32.Parse(W5);
            MaskTable[1, 2] = Int32.Parse(W6);
            MaskTable[2, 0] = Int32.Parse(W7);
            MaskTable[2, 1] = Int32.Parse(W8);
            MaskTable[2, 2] = Int32.Parse(W9);

            var maskSum = 0;
            foreach (var obj in MaskTable)
            {
                maskSum += obj;
            }
            for (var i = 1; i < _width - 1; i++)
            {
                for (var j = 1; j < _height - 1; j++)
                {
                    var green = 0;
                    var blue = 0;
                    var red = green = blue = 0;
                    for (var x = i - 1; x <= i + 1; x++)
                    {
                        for (var y = j - 1; y <= j + 1; y++)
                        {
                            var x2 = 0;
                            var y2 = 0;
                            if (x == i) x2 = 1;
                            if (x < i) x2 = 0;
                            if (x > i) x2 = 2;
                            if (y == j) y2 = 1;
                            if (y < j) y2 = 0;
                            if (y > j) y2 = 2;
                            var index2 = ImageOperations.GetIndexOfPixel(x, y, _stride);
                            red += MaskTable[x2, y2] * _pixels[index2 + 2];
                            green += MaskTable[x2, y2] * _pixels[index2 + 1];
                            blue += MaskTable[x2, y2] * _pixels[index2];
                        }
                    }
                    if (maskSum != 0)
                    {
                        blue /= maskSum;
                        red /= maskSum;
                        green /= maskSum;
                    }
                    blue = Math.Max(0, Math.Min(255, blue));
                    red = Math.Max(0, Math.Min(255, red));
                    green = Math.Max(0, Math.Min(255, green));


                    ChangePixel(i, j, red, green, blue);
                }
            }

            DrawChangePicture();
        }

        private void CanExecuteLinearFilter(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        public void KuwaharFilter(object sender, ExecutedRoutedEventArgs e)
        {
            for (var x = 2; x < _width - 2; x++)
            {
                for (var y = 2; y < _height - 2; y++)
                {
                    var regions = new List<Region>
                    {
                        new Region(x - 1, y - 1),
                        new Region(x + 1, y - 1),
                        new Region(x - 1, y + 1),
                        new Region(x + 1, y + 1)
                    };
                    foreach (var region in regions)
                    {
                        region.AvgBrightnessRed = GetAvgBrigthtnessKuwahar(region.StartX, region.StartY, Colors.Red);
                        region.AvgBrightnessGreen = GetAvgBrigthtnessKuwahar(region.StartX, region.StartY, Colors.Green);
                        region.AvgBrightnessBlue = GetAvgBrigthtnessKuwahar(region.StartX, region.StartY, Colors.Blue);
                        region.VarianceRed = GetVarianceKuwahar(region.StartX, region.StartY, region.AvgBrightnessRed,
                            Colors.Red);
                        region.VarianceGreen = GetVarianceKuwahar(region.StartX, region.StartY,
                            region.AvgBrightnessGreen, Colors.Green);
                        region.VarianceBlue = GetVarianceKuwahar(region.StartX, region.StartY, region.AvgBrightnessBlue,
                            Colors.Blue);
                    }
                    var minRegionRed = regions.OrderBy(par => par.VarianceRed).FirstOrDefault();
                    var minRegionGreen = regions.OrderBy(par => par.VarianceGreen).FirstOrDefault();
                    var minRegionBlue = regions.OrderBy(par => par.VarianceBlue).FirstOrDefault();
                    ChangePixel(x, y, minRegionRed.AvgBrightnessRed, minRegionGreen.AvgBrightnessGreen,
                        minRegionBlue.AvgBrightnessBlue);
                }
            }
            DrawChangePicture();
        }

        private void CanExecuteKuwaharFilter(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        public void MedianFilter(object sender, ExecutedRoutedEventArgs e)
        {
            int mask = Int32.Parse(_maskSize);

            for (var x = mask / 2; x < _width - mask / 2; x++)
            {
                for (var y = mask / 2; y < _height - mask / 2; y++)
                {
                    var middleR = GetMiddlePixel(x, y, mask, Colors.Red);
                    var middleG = GetMiddlePixel(x, y, mask, Colors.Green);
                    var middleB = GetMiddlePixel(x, y, mask, Colors.Blue);
                    ChangePixel(x, y, middleR, middleG, middleB);
                }
                ;
            }
            DrawChangePicture();
        }

        private void CanExecuteMedianFilter(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void BinarizationNiblackSettings(object sender, ExecutedRoutedEventArgs e)
        {
            binarizationNiblack.Visibility = Visibility.Visible;
            binarizationPanel.Visibility = Visibility.Hidden;
            stretchingPanel.Visibility = Visibility.Hidden;
            brightnessPanel.Visibility = Visibility.Hidden;
            histogramPanel.Visibility = Visibility.Hidden;
            filterPanel.Visibility = Visibility.Hidden;
            BinarizationThreshold = "-0.7";
            Frame = "15";
        }

        private void CanExecuteBinarizationNiblackSettings(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void BinarizationWithNiblack(object sender, ExecutedRoutedEventArgs e)
        {
            MakeGray();
            ChangePictureNiblack();
            DrawChangePicture();
        }

        private void CanExecuteBinarizationWithNiblack(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void BinarizationWithOtsu(object sender, ExecutedRoutedEventArgs e)
        {
            MakeGray();

            _histogram = new Histogram(_changedPixels);

            var threshold = GetOtsuThreshold(_histogram.GetHistogramAverageValues(),
                _changedPixels.Length / 4);

            var LUT = GetLUTForBinarization(threshold);

            ChangePictureWithLUT(LUT);

            DrawChangePicture();

            ShowHistogram(sender, e);
        }

        private void CanExecuteBinarizationWithOtsu(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void BinarizationWithThreshold(object sender, ExecutedRoutedEventArgs e)
        {
            MakeGray();
            var LUT = GetLUTForBinarization(int.Parse(BinarizationThreshold));

            ChangePictureWithLUT(LUT);

            DrawChangePicture();
        }

        private void CanExecuteBinarizationWithThreshold(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void BinarizationSettings(object sender, ExecutedRoutedEventArgs e)
        {
            binarizationPanel.Visibility = Visibility.Visible;
            binarizationNiblack.Visibility = Visibility.Hidden;
            stretchingPanel.Visibility = Visibility.Hidden;
            brightnessPanel.Visibility = Visibility.Hidden;
            histogramPanel.Visibility = Visibility.Hidden;
            filterPanel.Visibility = Visibility.Hidden;
            BinarizationThreshold = "20";
        }

        private void CanExecuteBinarizationSettings(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void MakeImageGray(object sender, ExecutedRoutedEventArgs e)
        {
            MakeGray();

            ShowHistogram(sender, e);
        }

        private void CanExecuteMakeImageGray(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void HistogramEqualization(object sender, ExecutedRoutedEventArgs e)
        {
            _histogram = new Histogram(_changedPixels);

            var Red = _histogram.GetHistogramRedValues();
            var Green = _histogram.GetHistogramGreenValues();
            var Blue = _histogram.GetHistogramBlueValues();

            var CR = ImageOperations.GetCumulativeHistogram(Red);
            var CG = ImageOperations.GetCumulativeHistogram(Green);
            var CB = ImageOperations.GetCumulativeHistogram(Blue);

            var LUTR = ImageOperations.GetLUTFromCumulativeHistogram(CR, _width, _height);
            var LUTG = ImageOperations.GetLUTFromCumulativeHistogram(CG, _width, _height);
            var LUTB = ImageOperations.GetLUTFromCumulativeHistogram(CB, _width, _height);

            ChangePictureWithLUT(LUTR, LUTG, LUTB);

            DrawChangePicture();

            ShowHistogram(sender, e);
        }

        private void CanExecuteHistogramEqualization(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void StretchingSettings(object sender, ExecutedRoutedEventArgs e)
        {
            stretchingPanel.Visibility = Visibility.Visible;
            binarizationNiblack.Visibility = Visibility.Hidden;
            brightnessPanel.Visibility = Visibility.Hidden;
            histogramPanel.Visibility = Visibility.Hidden;
            binarizationPanel.Visibility = Visibility.Hidden;
            filterPanel.Visibility = Visibility.Hidden;
        }

        private void CanExecuteStretchingSettings(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void Stretching(object sender, ExecutedRoutedEventArgs e)
        {
            var LUT = ImageOperations.GetLUTForHistogramStreching(_changedPixels, Convert.ToDouble(AA), Convert.ToDouble(BB));

            ChangePictureWithLUT(LUT);

            DrawChangePicture();

            ShowHistogram(sender, e);
        }

        private void CanExecuteStretching(object sender, CanExecuteRoutedEventArgs e)
        {
            int aaa;
            int bbb;
            if (!int.TryParse(aa.Text, out aaa)) aaa = -1;
            else aaa = int.Parse(aa.Text);
            if (!int.TryParse(bb.Text, out bbb)) bbb = -1;
            else bbb = int.Parse(bb.Text);
            if (_image != null && _aa!=null && _bb!=null && aaa>=0 && bbb>=0 && aaa<bbb) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void ChangeBrightness(object sender, RoutedEventArgs e)
        {
            SetBrightness(double.Parse(_brightness, System.Globalization.CultureInfo.InvariantCulture));
        }

        private void ChangeBrightness2(object sender, RoutedEventArgs e)
        {
            SetBrightness2(double.Parse(_brightness2, System.Globalization.CultureInfo.InvariantCulture));
        }

        private void BrightnessSettings(object sender, ExecutedRoutedEventArgs e)
        {
            brightnessPanel.Visibility = Visibility.Visible;
            binarizationNiblack.Visibility = Visibility.Hidden;
            histogramPanel.Visibility = Visibility.Hidden;
            stretchingPanel.Visibility = Visibility.Hidden;
            binarizationPanel.Visibility = Visibility.Hidden;
            filterPanel.Visibility = Visibility.Hidden;
        }

        private void CanExecuteBrightnessSettings(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void ShowHistogram(object sender, ExecutedRoutedEventArgs e)
        {
            _histogram = new Histogram(_changedPixels);
            LuminanceHistogramPoints = _histogram.luminanceHistogramPoints;
            RedColorHistogramPoints = _histogram.redColorHistogramPoints;
            GreenColorHistogramPoints = _histogram.greenColorHistogramPoints;
            BlueColorHistogramPoints = _histogram.blueColorHistogramPoints;

            histogramPanel.Visibility = Visibility.Visible;
            binarizationNiblack.Visibility = Visibility.Hidden;
            brightnessPanel.Visibility = Visibility.Hidden;
            stretchingPanel.Visibility = Visibility.Hidden;
            binarizationPanel.Visibility = Visibility.Hidden;
            filterPanel.Visibility = Visibility.Hidden;
        }

        private void CanExecuteShowHistogram(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void ChangeRGB(object sender, ExecutedRoutedEventArgs e)
        {
            ChangePixelRGB();
        }

        private void CanExecuteChangeRGB(object sender, CanExecuteRoutedEventArgs e)
        {
            int rr;
            int gg;
            int bb;
            if (!int.TryParse(r.Text, out rr)) rr = -1;
            else rr = int.Parse(r.Text);
            if (!int.TryParse(g.Text, out gg)) gg = -1;
            else gg = int.Parse(g.Text);
            if (!int.TryParse(b.Text, out bb)) bb = -1;
            else bb = int.Parse(b.Text);
            if (rr <= 255 && rr >= 0 && gg >= 0 && gg <= 255 && bb >= 0 && bb <= 255 && XYPixelValidation()) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void SaveFile(object sender, ExecutedRoutedEventArgs e)
        {
            SaveImageFile();
        }

        private void CanExecuteSaveFile(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image!=null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        private void OpenFile(object sender, ExecutedRoutedEventArgs e)
        {
            OpenImageFile();
            brightnessPanel.Visibility = Visibility.Hidden;
            binarizationNiblack.Visibility = Visibility.Hidden;
            histogramPanel.Visibility = Visibility.Hidden;
            stretchingPanel.Visibility = Visibility.Hidden;
            binarizationPanel.Visibility = Visibility.Hidden;
            filterPanel.Visibility = Visibility.Hidden;
        }

        private void CanExecuteOpenFile(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void GetPixel(object sender, ExecutedRoutedEventArgs e)
        {
            ToolPixel();
        }

        private void CanExecuteGetPixel(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_image != null) e.CanExecute = true;
            else e.CanExecute = false;
        }

        public void DrawChangePicture()
        {
            var rect = new Int32Rect(0, 0, _width, _height);
            Bitmap.WritePixels(rect, _changedPixels, _stride, 0);
        }

        public void DrawChangePicture(byte[] pixels)
        {
            var rect = new Int32Rect(0, 0, _width, _height);
            Bitmap.WritePixels(rect, pixels, _stride, 0);
        }

        #endregion

        #region RGB Validation

        public string Error
        {
            get { return String.Empty; }
        }

        public string this[string fieldName]
        {
            get
            {
                string result = null;
                int i;
                if (fieldName == "R1")
                {
                    if (int.TryParse(R1, out i))
                    {
                        if (i > 255 || i < 0)
                            result = "Wprowadź liczbę z zakresu 0-255";
                    }
                    else if(R1 != "" && R1 != null) result = "Wpisz liczbę";
                }
                if (fieldName == "G1")
                {
                    if (int.TryParse(G1, out i))
                    {
                        if (i > 255 || i < 0)
                            result = "Wprowadź liczbę z zakresu 0-255";
                    }
                    else if (G1 != "" && G1 != null) result = "Wpisz liczbę";
                }
                if (fieldName == "B1")
                {
                    if (int.TryParse(B1, out i))
                    {
                        if (i > 255 || i < 0)
                            result = "Wprowadź liczbę z zakresu 0-255";
                    }
                    else if (B1 != "" && B1 != null) result = "Wpisz liczbę";
                }
                if (fieldName == "XPixel")
                {
                    if (int.TryParse(XPixel, out i))
                    {
                        if (_imageProperties.Width == 0) result = "Otwórz jakiś plik";
                        else if (i > _imageProperties.Width || i < 0)
                            result = "Wpisz liczbę z zakresu 0-" + (_imageProperties.Width-1);
                    }
                    else if (XPixel != "" && XPixel != null) result = "Wpisz liczbę";
                }
                if (fieldName == "YPixel")
                {
                    if (int.TryParse(YPixel, out i))
                    {
                        if (_imageProperties.Height == 0) result = "Otwórz jakiś plik";
                        else if (i > _imageProperties.Height || i < 0)
                            result = "Wpisz liczbę z zakresu 0-" + (_imageProperties.Height-1);
                    }
                    else if (YPixel != "" && YPixel != null) result = "Wpisz liczbę";
                }
                return result;
            }
        }

        private bool XYPixelValidation()
        {
            int xx;
            int yy;
            if (!int.TryParse(x.Text, out xx)) xx = -1;
            else xx = int.Parse(x.Text);
            if (!int.TryParse(y.Text, out yy)) yy = -1;
            else yy = int.Parse(y.Text);
            if (xx >= 0 && xx < _imageProperties.Width && yy >= 0 && yy < _imageProperties.Height) return true;
            else return false;
        }

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            _imageProperties = new ImageProperties();
            DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region File open/save

        private void OpenImageFile()
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png;*.bmp;*.gif;*.tiff;*.tif|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "PNG (*.png)|*.png|" +
              "BMP (*.bmp)|*.bmp|" +
              "GIF (*.gif)|*.gif|" +
              "TIFF (*.tiff;*.tif)|*.tiff;*.tif";
            if (op.ShowDialog() == true)
            {
                Source = op.FileName;
                if (Source == null) return;

                _source = ImageValidation.Check8bppFormat(_source);

                _image = new BitmapImage(new Uri(_source));
                _imageProperties.Image = new BitmapImage(new Uri(_source));

                GetDimnesions();

                _bitmap.CopyPixels(_pixels, _stride, 0);
                _bitmap.CopyPixels(_changedPixels, _stride, 0);

                _imageProperties.Bitmap.CopyPixels(_imageProperties.Pixels, _imageProperties.Stride, 0);

                TransformGroup group = new TransformGroup();

                ScaleTransform xform = new ScaleTransform();
                group.Children.Add(xform);

                TranslateTransform tt = new TranslateTransform();
                TranslateTransform tt2 = new TranslateTransform();
                group.Children.Add(tt);
                group.Children.Add(tt2);

                imgBefore.RenderTransform = group;
                imgAfter.RenderTransform = group;

                border.MouseWheel += image_MouseWheel;
                border.MouseLeftButtonDown += image_MouseLeftButtonDown;
                border.MouseLeftButtonUp += image_MouseLeftButtonUp;
                border.MouseMove += image_MouseMove;

                border2.MouseWheel += image_MouseWheel;
                border2.MouseLeftButtonDown += image2_MouseLeftButtonDown;
                border2.MouseLeftButtonUp += image2_MouseLeftButtonUp;
                border2.MouseMove += image2_MouseMove;
            }
        }

        private void SaveImageFile()
        {
            BitmapEncoder encoder;
            var browser = new SaveFileDialog();
            browser.Filter = "PNG (*.png)|*.png|" +
                "BMP (*.bmp)|*.bmp|" +
                "GIF (*.gif)|*.gif|" +
                "JPEG (*.jpeg;*.jpg)|*.jpeg;*.jpg|" +
                "TIFF (*.tiff;*.tif)|*.tiff;*.tif";
            if (browser.ShowDialog() != true) return;
            else
            {
                var source = browser.FileName;
                if (source.EndsWith("gif"))
                {
                    encoder = new GifBitmapEncoder();
                }
                else if (source.EndsWith("jpeg") || source.EndsWith("jpg"))
                {
                    encoder = new JpegBitmapEncoder();
                }
                else if (source.EndsWith("tiff") || source.EndsWith("tif"))
                {
                    encoder = new TiffBitmapEncoder();
                }
                else if (source.EndsWith("png"))
                {
                    encoder = new PngBitmapEncoder();
                }
                else encoder = new BmpBitmapEncoder();

                encoder.Frames.Add(BitmapFrame.Create((BitmapSource)imgAfter.Source));
                using (var stream = new FileStream(source, FileMode.Create))
                encoder.Save(stream);
            }
        }

        private void GetDimnesions()
        {
            var SourceData = (BitmapSource)imgBefore.Source;
            _width = SourceData.PixelWidth;
            _height = SourceData.PixelHeight;
            Bitmap = new WriteableBitmap(_image);
            _stride = (_bitmap.PixelWidth * _bitmap.Format.BitsPerPixel + 7) / 8;
            _arraySize = _stride * _height;
            _pixels = new byte[_arraySize];
            _changedPixels = new byte[_arraySize];
            _imageProperties.Width = SourceData.PixelWidth;
            _imageProperties.Height = SourceData.PixelHeight;
            _imageProperties.Stride = (_bitmap.PixelWidth * _bitmap.Format.BitsPerPixel + 7) / 8;
            _imageProperties.ArraySize = _stride * _height;
            _imageProperties.Pixels = new byte[_arraySize];
            _imageProperties.ChangedPixels = new byte[_arraySize];
            _imageProperties.Bitmap = new WriteableBitmap(_image);

            _imageOperations = new ImageOperations { imgProperties = _imageProperties };
        }

        #endregion

        #region Zoom

        private void image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            imgBefore.ReleaseMouseCapture();
        }

        private void image2_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            imgAfter.ReleaseMouseCapture();
        }

        private void image_MouseMove(object sender, MouseEventArgs e)
        {
            if (!imgBefore.IsMouseCaptured) return;

            var tt = (TranslateTransform)((TransformGroup)imgBefore.RenderTransform).Children.First(tr => tr is TranslateTransform);
            var tt2 = (TranslateTransform)((TransformGroup)imgAfter.RenderTransform).Children.First(tr => tr is TranslateTransform);
            Vector v = start - e.GetPosition(border);
            tt.X = origin.X - v.X;
            tt.Y = origin.Y - v.Y;
            tt2.X = origin.X - v.X;
            tt2.Y = origin.Y - v.Y;
        }

        private void image2_MouseMove(object sender, MouseEventArgs e)
        {
            if (!imgAfter.IsMouseCaptured) return;

            var tt = (TranslateTransform)((TransformGroup)imgBefore.RenderTransform).Children.First(tr => tr is TranslateTransform);
            var tt2 = (TranslateTransform)((TransformGroup)imgAfter.RenderTransform).Children.First(tr => tr is TranslateTransform);
            Vector v = start - e.GetPosition(border2);
            tt.X = origin.X - v.X;
            tt.Y = origin.Y - v.Y;
            tt2.X = origin.X - v.X;
            tt2.Y = origin.Y - v.Y;
        }

        private void image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            imgBefore.CaptureMouse();

            var tt = (TranslateTransform)((TransformGroup)imgBefore.RenderTransform).Children.First(tr => tr is TranslateTransform);
            start = e.GetPosition(border);
            origin = new Point(tt.X, tt.Y);

            Point cp = e.GetPosition(imgBefore);
            int x = (int)((_imageProperties.Width / imgBefore.ActualWidth) * cp.X);
            int y = (int)((_imageProperties.Height / imgBefore.ActualHeight) * cp.Y);
            XPixel = x.ToString();
            YPixel = y.ToString();
        }

        private void image2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            imgAfter.CaptureMouse();

            var tt = (TranslateTransform)((TransformGroup)imgAfter.RenderTransform).Children.First(tr => tr is TranslateTransform);
            start = e.GetPosition(border2);
            origin = new Point(tt.X, tt.Y);

            Point cp = e.GetPosition(imgAfter);
            int x = (int)((_imageProperties.Width / imgAfter.ActualWidth) * cp.X);
            int y = (int)((_imageProperties.Height / imgAfter.ActualHeight) * cp.Y);
            XPixel = x.ToString();
            YPixel = y.ToString();
        }

        private void image_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            TransformGroup transformGroup = (TransformGroup)imgBefore.RenderTransform;
            ScaleTransform transform = (ScaleTransform)transformGroup.Children[0];

            double zoom = e.Delta > 0 ? .2 : -.2;
            transform.ScaleX += zoom;
            transform.ScaleY += zoom;
        }

        #endregion

        #region Pixel

        private void ToolPixel()
        {
            imgBefore.MouseLeftButtonDown += imgBefore_MouseLeftButtonDown;
            imgBefore.MouseLeftButtonUp += imgBefore_MouseLeftButtonUp;

            pixel.Visibility = Visibility.Visible;
        }

        private void HideDockPanel(object sender, RoutedEventArgs e)
        {
            pixel.Visibility = Visibility.Hidden;
        }

        private void imgBefore_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            imgBefore.ReleaseMouseCapture();
        }

        private void imgBefore_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            imgBefore.CaptureMouse();
        }

        public void ChangePixel(int x, int y, int r, int g, int b)
        {
            try
            {
                var index = ImageOperations.GetIndexOfPixel(x, y, _stride);
                _changedPixels[index + 2] = (byte)r;
                _changedPixels[index + 1] = (byte)g;
                _changedPixels[index] = (byte)b;
            }
            catch
            {
                return;
            }
        }

        private void ChangePixelRGB()
        {
            ChangePixel(Int32.Parse(XPixel), Int32.Parse(YPixel), Int32.Parse(R1), Int32.Parse(G1), Int32.Parse(B1));

            DrawChangePicture();
        }

        private void ShowPixelRGB()
        {
            try
            {
                var index = ImageOperations.GetIndexOfPixel(Int32.Parse(XPixel), Int32.Parse(YPixel), _stride);
                R = Convert.ToString(_pixels[index + 2]);
                G = Convert.ToString(_pixels[index + 1]);
                B = Convert.ToString(_pixels[index]);
                R1 = Convert.ToString(_changedPixels[index + 2]);
                G1 = Convert.ToString(_changedPixels[index + 1]);
                B1 = Convert.ToString(_changedPixels[index]);
            }
            catch { }
            
        }

        #endregion

        #region LUT

        public void ChangePictureWithLUT(int[] LUT)
        {
            for (var i = 0; i < _width; i++)
            {
                for (var j = 0; j < _height; j++)
                {
                    var index = ImageOperations.GetIndexOfPixel(i, j, _stride);

                    ChangePixel(i, j, _changedPixels[index + 2], _changedPixels[index + 1], _changedPixels[index]);

                    _changedPixels[index + 2] = (byte)LUT[_changedPixels[index + 2]];
                    _changedPixels[index + 1] = (byte)LUT[_changedPixels[index + 1]];
                    _changedPixels[index] = (byte)LUT[_changedPixels[index]];
                }
            }
        }

        public void ChangePictureWithLUT(int[] R, int[] G, int[] B)
        {
            for (var i = 0; i < _width; i++)
            {
                for (var j = 0; j < _height; j++)
                {
                    var index = ImageOperations.GetIndexOfPixel(i, j, _stride);

                    ChangePixel(i, j, _changedPixels[index + 2], _changedPixels[index + 1], _changedPixels[index]);

                    _changedPixels[index + 2] = (byte)R[_changedPixels[index + 2]];
                    _changedPixels[index + 1] = (byte)G[_changedPixels[index + 1]];
                    _changedPixels[index] = (byte)B[_changedPixels[index]];
                }
            }
        }

        public void SetBrightness(double brightnessValue)
        {
            var LUT = ImageOperations.GetLUTForChangeTheBrightness(Convert.ToDouble(brightnessValue));
            if (_pixels != null)
            {
                Array.Copy(_pixels, _changedPixels, _pixels.Length);

                ChangePictureWithLUT(LUT);

                DrawChangePicture();
            }
        }

        public void SetBrightness2(double brightnessValue)
        {
            var LUT = ImageOperations.GetLUTForChangeTheBrightness2(Convert.ToDouble(brightnessValue));
            if (_pixels != null)
            {
                Array.Copy(_pixels, _changedPixels, _pixels.Length);

                ChangePictureWithLUT(LUT);

                DrawChangePicture();
            }
        }

        #endregion

        #region Otsu, Niblack

        private void MakeGray()
        {
            for (var i = 0; i < _width; i++)
            {
                for (var j = 0; j < _height; j++)
                {
                    var index = ImageOperations.GetIndexOfPixel(i, j, _stride);

                    var color = (_changedPixels[index] + _changedPixels[index + 1] + _changedPixels[index + 2]) / 3;
                    _changedPixels[index + 2] = (byte)color;
                    _changedPixels[index + 1] = (byte)color;
                    _changedPixels[index] = (byte)color;
                }
            }
            DrawChangePicture();
        }

        public static int[] GetLUTForBinarization(int threshold)
        {
            var LUT = new int[256];

            for (var i = 0; i <= 255; i++)
            {
                LUT[i] = (i < threshold) ? 0 : 255;
            }

            return LUT;
        }

        public static int GetOtsuThreshold(int[] histData, int total)
        {
            float sum = 0;
            for (int t = 0; t < 256; t++) sum += t * histData[t];

            float sumB = 0;
            var wB = 0;
            var wF = 0;

            float varMax = 0;
            var threshold = 0;

            for (int t = 0; t < 256; t++)
            {
                wB += histData[t];
                if (wB == 0) continue;

                wF = total - wB;
                if (wF == 0) break;

                sumB += (float)(t * histData[t]);

                var mB = sumB / wB;
                var mF = (sum - sumB) / wF;

                var varBetween = (float)wB * (float)wF * (mB - mF) * (mB - mF);

                if (varBetween > varMax)
                {
                    varMax = varBetween;
                    threshold = t;
                }
            }

            return threshold;
        }

        public void ChangePictureNiblack()
        {
            for (var i = 0; i < _width; i++)
            {
                for (var j = 0; j < _height; j++)
                {
                    var index = ImageOperations.GetIndexOfPixel(i, j, _stride);
                    var treshold = CalculateTreshold(i, j, int.Parse(_frame), int.Parse(_frame));

                    if (_changedPixels[index] < treshold)
                    {
                        _changedPixels[index] = 0;
                        _changedPixels[index + 1] = 0;
                        _changedPixels[index + 2] = 0;
                    }
                    else
                    {
                        _changedPixels[index] = 255;
                        _changedPixels[index + 1] = 255;
                        _changedPixels[index + 2] = 255;
                    }
                }
            }
        }

        public int CalculateTreshold(int x, int y, int sideA, int sideB)
        {
            var avg = GetAverageColor(x, y, sideA, sideB);
            var odch = StandardDeviation(x, y, sideA, sideB, avg);
            var prog = avg + double.Parse(_binarizationThreshold, System.Globalization.CultureInfo.InvariantCulture) * odch;
            return (int)prog;
        }

        public double GetAverageColor(int x, int y, int sideA, int sideB)
        {
            double sum = 0;
            sideA = sideA / 2;
            sideB = sideB / 2;
            var startX = x - sideA >= 0 ? x - sideA : 0;
            var startY = y - sideB >= 0 ? y - sideB : 0;
            var endX = x + sideA <= _width ? x + sideA : _width;
            var endY = y + sideB <= _height ? y + sideB : _height;
            var counter = (endY - startY) * (endX - startX);
            for (var i = startX; i < endX; i++)
            {
                for (var j = startY; j < endY; j++)
                {
                    var index = ImageOperations.GetIndexOfPixel(i, j, _stride);
                    sum += _pixels[index];
                }
            }
            return sum / counter;
        }

        public double StandardDeviation(int x, int y, int sideA, int sideB, double avg)
        {
            double variance = 0;
            sideA = sideA / 2;
            sideB = sideB / 2;
            var startX = x - sideA >= 0 ? x - sideA : 0;
            var startY = y - sideB >= 0 ? y - sideB : 0;
            var endX = x + sideA <= _width ? x + sideA : _width;
            var endY = y + sideB <= _height ? y + sideB : _height;
            var counter = (endY - startY) * (endX - startX);

            for (var i = startX; i < endX; i++)
            {
                for (var j = startY; j < endY; j++)
                {
                    var index = ImageOperations.GetIndexOfPixel(i, j, _stride);
                    variance += (_pixels[index] - avg) * (_pixels[index] - avg);
                }
            }
            variance = variance / counter;
            return Math.Sqrt(variance);
        }

        #endregion

        #region Filters

        public int GetAvgBrigthtnessKuwahar(int startX, int startY, Colors color)
        {
            var avgBrightness = 0;
            for (var i = startX - 1; i <= startX + 1; i++)
            {
                for (var j = startY - 1; j <= startY + 1; j++)
                {
                    var index = ImageOperations.GetIndexOfPixel(i, j, _stride) + (int)color;
                    avgBrightness += _changedPixels[index];
                }
            }
            return avgBrightness / 9;
        }

        public double GetVarianceKuwahar(int startX, int startY, int avgBrightness, Colors color)
        {
            double variance = 0;
            for (var i = startX - 1; i <= startX + 1; i++)
            {
                for (var j = startY - 1; j <= startY + 1; j++)
                {
                    var index = ImageOperations.GetIndexOfPixel(i, j, _stride) + (int)color;
                    variance += (avgBrightness - _changedPixels[index]) * (avgBrightness - _changedPixels[index]);
                }
            }
            return variance / 9;
        }

        public int GetMiddlePixel(int startX, int startY, int mask, Colors color)
        {
            var maskPixels = new List<int>();
            for (var i = startX - mask / 2; i <= startX + mask / 2; i++)
            {
                for (var j = startY - mask / 2; j <= startY + mask / 2; j++)
                {
                    var index = ImageOperations.GetIndexOfPixel(i, j, _stride);
                    maskPixels.Add(_changedPixels[index + (int)color]);
                }
            }
            return maskPixels.OrderBy(x => x).Skip(maskPixels.Count / 2).First();
        }

        #endregion

        #region Thinning

        private void MinutiaeDecoration(List<Point> points, int sizeOfSquare)
        {
            foreach (var point in points)
            {

                for (int x = (int)point.X - sizeOfSquare / 2; x <= (int)point.X + sizeOfSquare / 2; x++)
                {
                    ChangePixel(x, (int)point.Y - sizeOfSquare / 2, 0, 0, 255);
                }
                for (int x = ((int)point.X - sizeOfSquare / 2) + 1; x <= ((int)point.X + sizeOfSquare / 2) - 1; x++)
                {
                    ChangePixel(x, (int)point.Y + sizeOfSquare / 2, 0, 0, 255);
                }
                for (int y = ((int)point.Y - sizeOfSquare / 2) + 1; y <= (int)point.Y + sizeOfSquare / 2; y++)
                {
                    ChangePixel((int)point.X - sizeOfSquare / 2, y, 0, 0, 255);
                }
                for (int y = ((int)point.Y - sizeOfSquare / 2) + 1; y <= (int)point.Y + sizeOfSquare / 2; y++)
                {
                    ChangePixel((int)point.X + sizeOfSquare / 2, y, 0, 0, 255);
                }
            }
            DrawChangePicture();
        }

        #endregion
    }
}
