﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO.Compression;
using Microsoft.Win32;

namespace projekt_biometria.Classes
{
    static class DebugFolder
    {
        public static string GetApplicationPath()
        {
            string path = System.IO.Path.GetDirectoryName(
              System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            return path.Remove(0, 6) + @"\Help.jpg";
        }

        public static string GetPath()
        {
            var browser = new OpenFileDialog();
            if (browser.ShowDialog() != true) return null;
            return browser.FileName;
        }

        public static string GetSavePath()
        {
            var browser = new SaveFileDialog();
            browser.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Tiff Image (.tiff)|*.tiff";
            if (browser.ShowDialog() != true) return null;
            return browser.FileName;
        }

        public static string GetSavePathToZip()
        {
            var browser = new SaveFileDialog();
            if (browser.ShowDialog() != true) return null;
            return browser.FileName;
        }
    }
}
