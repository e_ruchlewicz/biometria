﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projekt_biometria.Classes
{
    class Thinning
    {
        public static T[][] ArrayClone<T>(T[][] A)
        {
            return A.Select(a => a.ToArray()).ToArray();
        }

        public static bool[][] ZhangSuenThinning(bool[][] s)
        {
            var temp = ArrayClone(s);
            var count = 0;
            do
            {
                count = Step(1, temp, s);
                temp = ArrayClone(s);
                count += Step(2, temp, s);
                temp = ArrayClone(s);
            } while (count > 0);

            return s;
        }

        private static int Step(int stepNo, bool[][] temp, bool[][] s)
        {
            var count = 0;

            for (var a = 1; a < temp.Length - 1; a++)
            {
                for (var b = 1; b < temp[0].Length - 1; b++)
                {
                    if (SuenThinningAlg(a, b, temp, stepNo == 2))
                    {
                        if (s[a][b]) count++;
                        s[a][b] = false;
                    }
                }
            }
            return count;
        }

        private static bool SuenThinningAlg(int x, int y, bool[][] s, bool even)
        {
            var p2 = s[x][y - 1];
            var p3 = s[x + 1][y - 1];
            var p4 = s[x + 1][y];
            var p5 = s[x + 1][y + 1];
            var p6 = s[x][y + 1];
            var p7 = s[x - 1][y + 1];
            var p8 = s[x - 1][y];
            var p9 = s[x - 1][y - 1];


            var bp1 = NumberOfNonZeroNeighbors(x, y, s);
            if (bp1 >= 2 && bp1 <= 6)
            {
                if (NumberOfZeroToOneTransitionFromP9(x, y, s) == 1)
                {
                    if (even)
                    {
                        if (!(p2 && p4 && p8))
                        {
                            if (!(p2 && p6 && p8))
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        if (!(p2 && p4 && p6))
                        {
                            if (!(p4 && p6 && p8))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        private static int NumberOfZeroToOneTransitionFromP9(int x, int y, bool[][] s)
        {
            var p2 = s[x][y - 1];
            var p3 = s[x + 1][y - 1];
            var p4 = s[x + 1][y];
            var p5 = s[x + 1][y + 1];
            var p6 = s[x][y + 1];
            var p7 = s[x - 1][y + 1];
            var p8 = s[x - 1][y];
            var p9 = s[x - 1][y - 1];

            var A = Convert.ToInt32(!p2 && p3) + Convert.ToInt32(!p3 && p4) +
                    Convert.ToInt32(!p4 && p5) + Convert.ToInt32(!p5 && p6) +
                    Convert.ToInt32(!p6 && p7) + Convert.ToInt32(!p7 && p8) +
                    Convert.ToInt32(!p8 && p9) + Convert.ToInt32(!p9 && p2);
            return A;
        }

        private static int NumberOfNonZeroNeighbors(int x, int y, bool[][] s)
        {
            var count = 0;
            if (s[x - 1][y]) count++;
            if (s[x - 1][y + 1]) count++;
            if (s[x - 1][y - 1]) count++;
            if (s[x][y + 1]) count++;
            if (s[x][y - 1]) count++;
            if (s[x + 1][y]) count++;
            if (s[x + 1][y + 1]) count++;
            if (s[x + 1][y - 1]) count++;
            return count;
        }

        public static bool[][] KMMMThinning(bool[][] s)
        {
            int[,] deletionArray = {
                { 3, 5, 7, 12, 13, 14, 15, 20 },
{21  ,22 , 23 , 28  ,29 , 30 , 31  ,48 }   ,
{52  ,53 , 54 , 55  ,56 , 60 , 61  ,62 }   ,
{63  ,65 , 67 , 69  ,71 , 77 , 79  ,80 }   ,
{81  ,83 , 84 , 85  ,86 , 87 , 88  ,89 }   ,
{91  ,92 , 93 , 94  ,95 , 97 , 99  ,101}   ,
{103 ,109, 111, 112 ,113, 115, 116, 117},
{118 ,119, 120, 121 ,123, 124, 125, 126},
{127 ,131, 133, 135 ,141, 143, 149, 151},
{157 ,159, 181, 183 ,189, 191, 192, 193},
{195 ,197, 199, 205 ,207, 208, 209, 211},
{212 ,213, 214, 215 ,216, 217, 219, 220},
{221 ,222, 223, 224 ,225, 227, 229, 231},
{237 ,239, 240, 241 ,243, 244, 245, 246},
{ 247 ,248, 249, 251 ,252, 253, 254, 255 } };
            int[,] fourArrayValues = {
                { 3 , 6     , 12 , 24 ,48   , 96    , 192 , 129 },
                { 7 , 14    , 28 , 56 ,112  , 224   , 193 , 131 },
                { 15,  30   , 60 , 120, 240 , 225   , 195 , 135 } };
            var tab = new int[s.Length][];
            //zamiana na tablice intow 
            for (var y = 0; y < s.Length; y++)
            {
                tab[y] = new int[s[y].Length];
                for (var x = 0; x < s[y].Length; x++)
                {

                    tab[y][x] = s[y][x] ? 1 : 0;
                }
            }
            var countDeleted = 0;

            do
            {
                countDeleted = 0;
                //ustawienie px na 1
                for (var x = 0; x < tab.Length; x++)
                {
                    for (var y = 0; y < tab[x].Length; y++)
                    {
                        if (tab[x][y] > 0)
                            tab[x][y] = 1;
                    }
                }

                //ustawienie px stykajacych sie z tlem na 2
                for (var x = 1; x < tab.Length - 1; x++)
                {
                    for (var y = 1; y < tab[x].Length - 1; y++)
                    {


                        if (tab[x][y] > 0)
                        {
                            var p1 = tab[x - 1][y - 1] > 0 ? 1 : 0;
                            var p2 = tab[x - 1][y] > 0 ? 1 : 0;
                            var p3 = tab[x - 1][y + 1] > 0 ? 1 : 0;
                            var p4 = tab[x][y - 1] > 0 ? 1 : 0;
                            var p6 = tab[x][y + 1] > 0 ? 1 : 0;
                            var p7 = tab[x + 1][y - 1] > 0 ? 1 : 0;
                            var p8 = tab[x + 1][y] > 0 ? 1 : 0;
                            var p9 = tab[x + 1][y + 1] > 0 ? 1 : 0;
                            var sum = p1 + p2 + p3 + p4 + p6 + p7 + p8 + p9;
                            if (sum < 8)
                                tab[x][y] = 2;
                        }
                    }
                }
                //ustawienie px stykajacych sie z tlem rogami na 3ki
                for (var x = 1; x < tab.Length - 1; x++)
                {
                    for (var y = 1; y < tab[x].Length - 1; y++)
                    {

                        if (tab[x][y] > 1)
                        {
                            var p1 = tab[x - 1][y - 1] > 0 ? 1 : 0;
                            var p2 = tab[x - 1][y] > 0 ? 1 : 0;
                            var p3 = tab[x - 1][y + 1] > 0 ? 1 : 0;
                            var p4 = tab[x][y - 1] > 0 ? 1 : 0;
                            var p6 = tab[x][y + 1] > 0 ? 1 : 0;
                            var p7 = tab[x + 1][y - 1] > 0 ? 1 : 0;
                            var p8 = tab[x + 1][y] > 0 ? 1 : 0;
                            var p9 = tab[x + 1][y + 1] > 0 ? 1 : 0;
                            var sum = p1 + p2 + p3 + p4 + p6 + p7 + p8 + p9;
                            //px w srodku plusa
                            if (p2 + p4 + p6 + p8 == 4 && sum < 8)
                                tab[x][y] = 3;
                        }
                    }
                }
                //ustawienie px posiadajacych 2,3,4 przylegajacych sasiadow na 4ki
                for (var x = 1; x < tab.Length - 1; x++)
                {
                    for (var y = 1; y < tab[x].Length - 1; y++)
                    {

                        if (tab[x][y] > 0)
                        {
                            var p1 = tab[x - 1][y - 1] > 0 ? 128 : 0;
                            var p2 = tab[x - 1][y] > 0 ? 1 : 0;
                            var p3 = tab[x - 1][y + 1] > 0 ? 2 : 0;
                            var p4 = tab[x][y - 1] > 0 ? 64 : 0;
                            var p6 = tab[x][y + 1] > 0 ? 4 : 0;
                            var p7 = tab[x + 1][y - 1] > 0 ? 32 : 0;
                            var p8 = tab[x + 1][y] > 0 ? 16 : 0;
                            var p9 = tab[x + 1][y + 1] > 0 ? 8 : 0;
                            var sum = p1 + p2 + p3 + p4 + p6 + p7 + p8 + p9;
                            foreach (var e in fourArrayValues)
                                if (e == sum)
                                {
                                    tab[x][y] = 4;
                                    break;
                                }
                        }
                    }
                }

                //usuwanie wszystkich 4
                for (var x = 1; x < tab.Length - 1; x++)
                {
                    for (var y = 1; y < tab[x].Length - 1; y++)
                    {
                        if (tab[x][y] == 4)
                        {
                            tab[x][y] = 0;
                            countDeleted++;
                        }
                    }
                }
                //usuwanie 2 a nieusuniete ustawiane na 1
                for (var x = 1; x < tab.Length - 1; x++)
                {
                    for (var y = 1; y < tab[x].Length - 1; y++)
                    {

                        if (tab[x][y] == 2)
                        {

                            var p1 = tab[x - 1][y - 1] > 0 ? 1 : 0;
                            var p2 = tab[x - 1][y] > 0 ? 1 : 0;
                            var p3 = tab[x - 1][y + 1] > 0 ? 1 : 0;
                            var p4 = tab[x][y - 1] > 0 ? 1 : 0;
                            var p6 = tab[x][y + 1] > 0 ? 1 : 0;
                            var p7 = tab[x + 1][y - 1] > 0 ? 1 : 0;
                            var p8 = tab[x + 1][y] > 0 ? 1 : 0;
                            var p9 = tab[x + 1][y + 1] > 0 ? 1 : 0;
                            var sum = p1 * 128 + p2 * 1 + p3 * 2 + p4 * 64 + p6 * 4 + p7 * 32 + p8 * 16 + p9 * 8;
                            var iloscSasiadow = p1 + p2 + p3 + p4 + p6 + p7 + p8 + p9;
                            if (iloscSasiadow > 2)
                                foreach (var e in deletionArray)
                                    if (e == sum)
                                    {
                                        tab[x][y] = 0;
                                        countDeleted++;
                                        break;
                                    }
                            if (tab[x][y] == 2)
                                tab[x][y] = 1;
                        }
                    }
                }
                //usuwanie 3 a nieusuniete ustawiane na 1
                for (var x = 1; x < tab.Length - 1; x++)
                {
                    for (var y = 1; y < tab[x].Length - 1; y++)
                    {

                        if (tab[x][y] == 3)
                        {

                            var p1 = tab[x - 1][y - 1] > 0 ? 1 : 0;
                            var p2 = tab[x - 1][y] > 0 ? 1 : 0;
                            var p3 = tab[x - 1][y + 1] > 0 ? 1 : 0;
                            var p4 = tab[x][y - 1] > 0 ? 1 : 0;
                            var p6 = tab[x][y + 1] > 0 ? 1 : 0;
                            var p7 = tab[x + 1][y - 1] > 0 ? 1 : 0;
                            var p8 = tab[x + 1][y] > 0 ? 1 : 0;
                            var p9 = tab[x + 1][y + 1] > 0 ? 1 : 0;
                            var sum = p1 * 128 + p2 * 1 + p3 * 2 + p4 * 64 + p6 * 4 + p7 * 32 + p8 * 16 + p9 * 8;
                            var iloscSasiadow = p1 + p2 + p3 + p4 + p6 + p7 + p8 + p9;
                            if (iloscSasiadow > 2)
                                foreach (var e in deletionArray)
                                    if (e == sum)
                                    {
                                        tab[x][y] = 0;
                                        countDeleted++;
                                        break;
                                    }
                            if (tab[x][y] == 3)
                                tab[x][y] = 1;
                        }
                    }
                }



            } while ((countDeleted > 0));


            //zamiana na tablice booli 
            for (var y = 0; y < s.Length; y++)
            {
                for (var x = 0; x < s[y].Length; x++)
                {

                    s[y][x] = tab[y][x] > 0;
                }
            }


            return s;
        }
    }
}
