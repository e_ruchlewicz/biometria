﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace projekt_biometria
{
    public class DataCommands
    {
        private static RoutedUICommand showHistogram = new RoutedUICommand("ShowHistogram", "Show", typeof(DataCommands));
        private static RoutedUICommand changeRGB = new RoutedUICommand("ChangeRGB", "Change", typeof(DataCommands));
        private static RoutedUICommand saveFile = new RoutedUICommand("SaveFile", "Save", typeof(DataCommands));
        private static RoutedUICommand openFile = new RoutedUICommand("OpenFile", "Open", typeof(DataCommands));
        private static RoutedUICommand getPixel = new RoutedUICommand("GetPixel", "GetPixel", typeof(DataCommands));
        private static RoutedUICommand brightnessSettings = new RoutedUICommand("BrightnessSettings", "SetBrightness", typeof(DataCommands));
        private static RoutedUICommand stretching = new RoutedUICommand("Stretching", "Stretch", typeof(DataCommands));
        private static RoutedUICommand stretchingSettings = new RoutedUICommand("StretchingSettings", "SetStretching", typeof(DataCommands));
        private static RoutedUICommand histogramEqualization = new RoutedUICommand("HistogramEqualization", "HistogramEqualizen", typeof(DataCommands));
        private static RoutedUICommand makeImageGray = new RoutedUICommand("MakeImageGray", "MakeGray", typeof(DataCommands));
        private static RoutedUICommand binarizationWithThreshold = new RoutedUICommand("BinarizationWithThreshold", "Binarize", typeof(DataCommands));
        private static RoutedUICommand binarizationSettings = new RoutedUICommand("BinarizationSettings", "SetThreshold", typeof(DataCommands));
        private static RoutedUICommand binarizationWithOtsu = new RoutedUICommand("BinarizationWithOtsu", "Binarize", typeof(DataCommands));
        private static RoutedUICommand binarizationWithNiblack = new RoutedUICommand("BinarizationWithNiblack", "Binarize", typeof(DataCommands));
        private static RoutedUICommand binarizationNiblackSettings = new RoutedUICommand("BinarizationNiblackSettings", "SetThreshold", typeof(DataCommands));
        private static RoutedUICommand filterSettings = new RoutedUICommand("FilterSettings", "SetMask", typeof(DataCommands));
        private static RoutedUICommand linearFilter = new RoutedUICommand("LinearFilter", "Filter", typeof(DataCommands));
        private static RoutedUICommand kuwaharFilter = new RoutedUICommand("KuwaharFilter", "Filter", typeof(DataCommands));
        private static RoutedUICommand medianFilter = new RoutedUICommand("MedianFilter", "Filter", typeof(DataCommands));
        private static RoutedUICommand prewittFilter = new RoutedUICommand("PrewittFilter", "Filter", typeof(DataCommands));
        private static RoutedUICommand sobelFilter = new RoutedUICommand("SobelFilter", "Filter", typeof(DataCommands));
        private static RoutedUICommand laplaceFilter = new RoutedUICommand("LaplaceFilter", "Filter", typeof(DataCommands));
        private static RoutedUICommand blurFilter = new RoutedUICommand("BlurFilter", "Filter", typeof(DataCommands));
        private static RoutedUICommand cornerDetectingFilter = new RoutedUICommand("CornerDetectingFilter", "Filter", typeof(DataCommands));
        private static RoutedUICommand pictureThinning = new RoutedUICommand("PictureThinning", "Thinning", typeof(DataCommands));
        private static RoutedUICommand findingBranches = new RoutedUICommand("FindingBranches", "FindingBranches", typeof(DataCommands));
        private static RoutedUICommand findingEndings = new RoutedUICommand("FindingEndings", "FindingEndings", typeof(DataCommands));
        private static RoutedUICommand filteringEndings = new RoutedUICommand("FilteringEndings", "FilteringEndings", typeof(DataCommands));


        public static RoutedUICommand ShowHistogram
        {
            get { return showHistogram; }
        }

        public static RoutedUICommand ChangeRGB
        {
            get { return changeRGB; }
        }

        public static RoutedUICommand SaveFile
        {
            get { return saveFile; }
        }

        public static RoutedUICommand OpenFile
        {
            get { return openFile; }
        }

        public static RoutedUICommand GetPixel
        {
            get { return getPixel; }
        }

        public static RoutedUICommand BrightnessSettings
        {
            get { return brightnessSettings; }
        }

        public static RoutedUICommand Stretching
        {
            get { return stretching; }
        }

        public static RoutedUICommand StretchingSettings
        {
            get { return stretchingSettings; }
        }

        public static RoutedUICommand HistogramEqualization
        {
            get { return histogramEqualization; }
        }

        public static RoutedUICommand MakeImageGray
        {
            get { return makeImageGray; }
        }

        public static RoutedUICommand BinarizationWithThreshold
        {
            get { return binarizationWithThreshold; }
        }

        public static RoutedUICommand BinarizationSettings
        {
            get { return binarizationSettings; }
        }

        public static RoutedUICommand BinarizationWithOtsu
        {
            get { return binarizationWithOtsu; }
        }

        public static RoutedUICommand BinarizationWithNiblack
        {
            get { return binarizationWithNiblack; }
        }

        public static RoutedUICommand BinarizationNiblackSettings
        {
            get { return binarizationNiblackSettings; }
        }

        public static RoutedUICommand FilterSettings
        {
            get { return filterSettings; }
        }

        public static RoutedUICommand LinearFilter
        {
            get { return linearFilter; }
        }

        public static RoutedUICommand KuwaharFilter
        {
            get { return kuwaharFilter; }
        }

        public static RoutedUICommand MedianFilter
        {
            get { return medianFilter; }
        }

        public static RoutedUICommand PrewittFilter
        {
            get { return prewittFilter; }
        }

        public static RoutedUICommand SobelFilter
        {
            get { return sobelFilter; }
        }

        public static RoutedUICommand LaplaceFilter
        {
            get { return laplaceFilter; }
        }

        public static RoutedUICommand BlurFilter
        {
            get { return blurFilter; }
        }

        public static RoutedUICommand CornerDetectingFilter
        {
            get { return cornerDetectingFilter; }
        }

        public static RoutedUICommand PictureThinning
        {
            get { return pictureThinning; }
        }

        public static RoutedUICommand FindingBranches
        {
            get { return findingBranches; }
        }

        public static RoutedUICommand FindingEndings
        {
            get { return findingEndings; }
        }
        
        public static RoutedUICommand FilteringEndings
        {
            get { return filteringEndings; }
        }
    }
}
